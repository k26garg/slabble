
import 'dart:io';

import 'package:rxdart/rxdart.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/model/create_address_model.dart';

import 'bloc_base.dart';
import 'form_field_validators.dart';

class AddressBloc with Validators implements BlocBase {
  final _flatNo = BehaviorSubject<String>();
  final _landmark = new BehaviorSubject<String>();


  Stream<String> get getFlatNo => _flatNo.stream;
  Stream<String> get getLandmark => _landmark.stream;


  final loader = BehaviorSubject<bool>();

  Stream<bool> get loading => loader.stream;


  Function(String) get changeFlatNo => _flatNo.sink.add;

  Function(String) get changeLandmark => _landmark.sink.add;




  @override
  void dispose() {
    _flatNo.close();
    _landmark.close();
    loader.close();
  }

  CreateAddressModel createAddressModel() {
    final flatNo = _flatNo.value;
    final landmark = _landmark.value;


    if(checkAddressValidation(flatNo,landmark)){
      loader.sink.add(true);
      CreateAddressModel createAddressUi = CreateAddressModel(null,null,null,null,flatNo,landmark,null,null,null);

      return createAddressUi;
    }

    return null;

  }
}
