import 'dart:io';

import 'package:slabble/common/api_request.dart';
import 'package:rxdart/rxdart.dart';
import 'package:slabble/model/search_location_model.dart';
import 'bloc_base.dart';

class SearchLocationBloc implements BlocBase{

  var searchString =  new PublishSubject<String>();

  var locationResult = new BehaviorSubject<List<SearchLocationModel>>();


  Function(String) get changeSearchString => searchString.sink.add;

  ///get stream data
  Stream<String> get getSearchString => searchString.stream;

  Stream<List<SearchLocationModel>> get getLocationResult => locationResult.stream;


  Stream<dynamic> get getAddresses => Observable.fromFuture(getAllUserAddress());

  @override
  void dispose() {
    // TODO: implement dispose
    searchString.close();
    locationResult.close();
  }

  getLocationFromSearch(String value) async{
    return await ApiRequest.getLocationFromSearch(
        url: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$value&key=AIzaSyAyjW7yBPc3ZJXOxaCd2SpCvKxaIpooQHg",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'}).then((value){

          List response = value["predictions"];

          List<SearchLocationModel> searchResultList = response.map((m) => SearchLocationModel.fromJson(m)).toList();

          locationResult.sink.add(searchResultList);

    }).catchError((onError) {
      throw onError;
    });
  }

  getAllUserAddress() async{
    return await ApiRequest.allGetApi(
        url: "api/v1/user/address",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .catchError((onError) {
      throw onError;
    });

  }

}