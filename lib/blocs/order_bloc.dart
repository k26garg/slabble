import 'dart:io';

import 'package:rxdart/rxdart.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/model/cart.dart';

import 'bloc_base.dart';

class OrderBloc implements BlocBase{

  Stream<dynamic> get getAddresses => Observable.fromFuture(getAllUserAddress());
  var selectAddressId = new BehaviorSubject<int>();
  var showLoader = new BehaviorSubject<bool>.seeded(false);
  var cart = new BehaviorSubject<Cart>();

  Stream<int> get getSelectAddressId => selectAddressId.stream;

  var bedRoomCount =  new BehaviorSubject<int>();
  var bathRoomCount = new BehaviorSubject<int>();

  Stream<int> get getBedRoomCount => bedRoomCount.stream;
  Stream<int> get getBathRoomCount => bathRoomCount.stream;
  Stream<bool> get getLoader => showLoader.stream;
  Stream<Cart> get getCart => cart.stream;


  getAllUserAddress() async{
    return await ApiRequest.allGetApi(
        url: "api/v1/user/address",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .catchError((onError) {
      throw onError;
    });

  }

  @override
  void dispose() {
    // TODO: implement dispose
    selectAddressId.close();
    bedRoomCount.close();
    bathRoomCount.close();
    showLoader.close();
    cart.close();
  }

}