import 'dart:io';

import 'package:rxdart/rxdart.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/model/packages.dart';

import 'bloc_base.dart';

class ServiceBloc implements BlocBase{

  int packageId;

  var showLoader = new BehaviorSubject<bool>.seeded(false);


  ServiceBloc(this.packageId);

  Stream<dynamic> get getServiceList =>
      Observable.fromFuture(_getServices());

  Stream<bool> get getLoader => showLoader.stream;


  @override
  void dispose() {
    // TODO: implement dispose
    showLoader.close();
  }

  _getServices() async {
    return await ApiRequest.allGetApi(
        url: "api/v1/packages/$packageId/services",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .catchError((onError) {
      throw onError;
    });
  }

}