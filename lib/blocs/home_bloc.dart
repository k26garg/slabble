import 'dart:io';

import 'package:rxdart/rxdart.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/model/home_widget_model.dart';

import 'bloc_base.dart';

class HomeBloc implements BlocBase{

  Stream<dynamic> get getPackageList =>
      Observable.fromFuture(_getPackages());

  Stream<dynamic> get getServiceList =>
      Observable.fromFuture(_getServices());

  Stream<HomeWidgetModel> get homeService => Observable.combineLatest2(getPackageList,getServiceList,(A,B) => HomeWidgetModel(A,B));

  @override
  void dispose() {
    // TODO: implement dispose
  }

  _getPackages() async {
    return await ApiRequest.allGetApi(
        url: "api/v1/packages",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .catchError((onError) {
      throw onError;
    });
  }

  _getServices() async {
    return await ApiRequest.allGetApi(
        url: "api/v1/services",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .catchError((onError) {
      throw onError;
    });
  }

}