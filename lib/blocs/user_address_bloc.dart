import 'dart:io';
import 'package:slabble/common/api_request.dart';
import 'bloc_base.dart';
import 'package:rxdart/rxdart.dart';

class UserAddressBloc implements BlocBase{

  Stream<dynamic> get getAllAddress => Observable.fromFuture(getAllUserAddress());

  @override
  void dispose() {
    // TODO: implement dispose
  }

  getAllUserAddress() async{
    return await ApiRequest.allGetApi(
        url: "api/v1/user/address",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .catchError((onError) {
      throw onError;
    });

  }

}