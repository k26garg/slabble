import 'package:json_annotation/json_annotation.dart';

part 'base_response.g.dart';

@JsonSerializable()
class BaseResponse{
  int statusCode;
  String status;
  String message;
  String token;

  BaseResponse(this.statusCode, this.status, this.message,this.token);

  factory BaseResponse.fromJson(Map<String, dynamic> json) => _$BaseResponseFromJson(json);


  Map<String, dynamic> toJson() => _$BaseResponseToJson(this);


}