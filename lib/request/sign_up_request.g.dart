// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_up_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignUpRequest _$SignUpRequestFromJson(Map<String, dynamic> json) {
  return SignUpRequest(
      json['name'] as String,
      json['email'] as String,
      json['mobileNumber'] as String,
      json['imageUrl'] as String,
      json['loginType'] as String);
}

Map<String, dynamic> _$SignUpRequestToJson(SignUpRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
      'mobileNumber': instance.mobileNumber,
      'imageUrl': instance.imageUrl,
      'loginType': instance.loginType
    };
