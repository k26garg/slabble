import 'package:json_annotation/json_annotation.dart';

part 'sign_up_request.g.dart';

@JsonSerializable()
class SignUpRequest{

  String name;
  String email;
  String mobileNumber;
  String imageUrl;
  String loginType;

  SignUpRequest(this.name, this.email, this.mobileNumber,this.imageUrl,this.loginType);

  factory SignUpRequest.fromJson(Map<String, dynamic> json) => _$SignUpRequestFromJson(json);


  Map<String, dynamic> toJson() => _$SignUpRequestToJson(this);


}