import 'package:json_annotation/json_annotation.dart';

part 'create_cart_request.g.dart';

@JsonSerializable()
class CreateCartRequest {
  int packageId;
  int noOfBathRoom;
  int noOfBedRoom;

  CreateCartRequest(this.packageId, this.noOfBathRoom, this.noOfBedRoom);

  factory CreateCartRequest.fromJson(Map<String, dynamic> json) => _$CreateCartRequestFromJson(json);


  Map<String, dynamic> toJson() => _$CreateCartRequestToJson(this);


}