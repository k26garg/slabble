// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_cart_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateCartRequest _$CreateCartRequestFromJson(Map<String, dynamic> json) {
  return CreateCartRequest(json['packageId'] as int,
      json['noOfBathRoom'] as int, json['noOfBedRoom'] as int);
}

Map<String, dynamic> _$CreateCartRequestToJson(CreateCartRequest instance) =>
    <String, dynamic>{
      'packageId': instance.packageId,
      'noOfBathRoom': instance.noOfBathRoom,
      'noOfBedRoom': instance.noOfBedRoom
    };
