import 'package:json_annotation/json_annotation.dart';

part 'search_location_text_model.g.dart';

@JsonSerializable()
class SearchLocationTextModel{
  String main_text;
  String secondary_text;

  SearchLocationTextModel(this.main_text, this.secondary_text);

  factory SearchLocationTextModel.fromJson(Map<String, dynamic> json) => _$SearchLocationTextModelFromJson(json);


  Map<String, dynamic> toJson() => _$SearchLocationTextModelToJson(this);


}