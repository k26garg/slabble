import 'package:json_annotation/json_annotation.dart';

part 'packages.g.dart';

@JsonSerializable()
class Packages{

  int packageId;
  String name;
  double price;
  String imageUrl;
  int minBathRomCount;
  int minBedRoomCount;
  int bathRoomPerPrice;
  int bedRoomPerPrice;


  Packages(this.packageId, this.name, this.price, this.imageUrl,
      this.minBathRomCount, this.minBedRoomCount, this.bathRoomPerPrice,
      this.bedRoomPerPrice);

  factory Packages.fromJson(Map<String, dynamic> json) => _$PackagesFromJson(json);


  Map<String, dynamic> toJson() => _$PackagesToJson(this);
}