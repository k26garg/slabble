import 'package:json_annotation/json_annotation.dart';

import 'search_location_text_model.dart';

part 'search_location_model.g.dart';

@JsonSerializable()
class SearchLocationModel{

  String description;
  String place_id;
  SearchLocationTextModel structured_formatting;


  SearchLocationModel(this.description, this.structured_formatting);

  factory SearchLocationModel.fromJson(Map<String, dynamic> json) => _$SearchLocationModelFromJson(json);


  Map<String, dynamic> toJson() => _$SearchLocationModelToJson(this);
}