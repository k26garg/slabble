import 'package:json_annotation/json_annotation.dart';
import 'package:slabble/model/product.dart';

part 'cart_order_item.g.dart';

@JsonSerializable()
class CartOrderItem{
  int cartItemId;
  int no_of_bathroom;
  int no_of_bedroom;
  double totalAmount;
  double payableAmount;
  double regularAmount;
  Product product;

  CartOrderItem(this.cartItemId, this.no_of_bathroom, this.no_of_bedroom,
      this.totalAmount, this.payableAmount, this.regularAmount,this.product);

  factory CartOrderItem.fromJson(Map<String, dynamic> json) => _$CartOrderItemFromJson(json);


  Map<String, dynamic> toJson() => _$CartOrderItemToJson(this);


}