// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressResponse _$AddressResponseFromJson(Map<String, dynamic> json) {
  return AddressResponse(
      json['addressId'] as int, json['text'] as String, json['type'] as String);
}

Map<String, dynamic> _$AddressResponseToJson(AddressResponse instance) =>
    <String, dynamic>{
      'addressId': instance.addressId,
      'text': instance.text,
      'type': instance.type
    };
