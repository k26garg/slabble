import 'package:json_annotation/json_annotation.dart';

part 'product.g.dart';

@JsonSerializable()
class Product{
  int productId;
  String name;
  int commodityId;
  String imageUrl;
  double salePrice;
  double regularPrice;

  Product(this.productId, this.name, this.commodityId, this.imageUrl,
      this.salePrice, this.regularPrice);

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);


  Map<String, dynamic> toJson() => _$ProductToJson(this);


}