// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_order_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartOrderItem _$CartOrderItemFromJson(Map<String, dynamic> json) {
  return CartOrderItem(
      json['cartItemId'] as int,
      json['no_of_bathroom'] as int,
      json['no_of_bedroom'] as int,
      (json['totalAmount'] as num)?.toDouble(),
      (json['payableAmount'] as num)?.toDouble(),
      (json['regularAmount'] as num)?.toDouble(),
      json['product'] == null
          ? null
          : Product.fromJson(json['product'] as Map<String, dynamic>));
}

Map<String, dynamic> _$CartOrderItemToJson(CartOrderItem instance) =>
    <String, dynamic>{
      'cartItemId': instance.cartItemId,
      'no_of_bathroom': instance.no_of_bathroom,
      'no_of_bedroom': instance.no_of_bedroom,
      'totalAmount': instance.totalAmount,
      'payableAmount': instance.payableAmount,
      'regularAmount': instance.regularAmount,
      'product': instance.product
    };
