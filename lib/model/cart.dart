import 'package:json_annotation/json_annotation.dart';

import 'cart_order_item.dart';

part 'cart.g.dart';

@JsonSerializable()
class Cart{
  int cartId;
  List<CartOrderItem> items;

  Cart(this.cartId, this.items);

  factory Cart.fromJson(Map<String, dynamic> json) => _$CartFromJson(json);


  Map<String, dynamic> toJson() => _$CartToJson(this);


}