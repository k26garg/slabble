// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'packages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Packages _$PackagesFromJson(Map<String, dynamic> json) {
  return Packages(
      json['packageId'] as int,
      json['name'] as String,
      (json['price'] as num)?.toDouble(),
      json['imageUrl'] as String,
      json['minBathRomCount'] as int,
      json['minBedRoomCount'] as int,
      json['bathRoomPerPrice'] as int,
      json['bedRoomPerPrice'] as int);
}

Map<String, dynamic> _$PackagesToJson(Packages instance) => <String, dynamic>{
      'packageId': instance.packageId,
      'name': instance.name,
      'price': instance.price,
      'imageUrl': instance.imageUrl,
      'minBathRomCount': instance.minBathRomCount,
      'minBedRoomCount': instance.minBedRoomCount,
      'bathRoomPerPrice': instance.bathRoomPerPrice,
      'bedRoomPerPrice': instance.bedRoomPerPrice
    };
