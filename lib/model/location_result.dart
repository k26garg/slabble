class LocationResult{

  String _postalCode;
  String _country;
  String _state;
  String _city;
  String _description;
  String _latitude;
  String _longitude;

  String get postalCode => _postalCode;

  set postalCode(String value) {
    _postalCode = value;
  }

  String get country => _country;

  String get description => _description;

  set description(String value) {
    _description = value;
  }

  String get city => _city;

  set city(String value) {
    _city = value;
  }

  String get state => _state;

  set state(String value) {
    _state = value;
  }

  set country(String value) {
    _country = value;
  }

  String get longitude => _longitude;

  set longitude(String value) {
    _longitude = value;
  }

  String get latitude => _latitude;

  set latitude(String value) {
    _latitude = value;
  }


}