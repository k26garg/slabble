// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
      json['productId'] as int,
      json['name'] as String,
      json['commodityId'] as int,
      json['imageUrl'] as String,
      (json['salePrice'] as num)?.toDouble(),
      (json['regularPrice'] as num)?.toDouble());
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'productId': instance.productId,
      'name': instance.name,
      'commodityId': instance.commodityId,
      'imageUrl': instance.imageUrl,
      'salePrice': instance.salePrice,
      'regularPrice': instance.regularPrice
    };
