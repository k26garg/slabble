import 'package:json_annotation/json_annotation.dart';

part 'create_address_model.g.dart';

@JsonSerializable()
class CreateAddressModel{

  String addressText;
  String city;
  String flatNo;
  String landmark;
  String latitude;
  String longitude;
  String pincode;
  String state;
  String type;

  CreateAddressModel(this.addressText, this.city, this.pincode,
      this.state,this.flatNo,this.landmark,this.latitude,this.longitude,this.type);

  factory CreateAddressModel.fromJson(Map<String, dynamic> json) => _$CreateAddressModelFromJson(json);


  Map<String, dynamic> toJson() => _$CreateAddressModelToJson(this);


}