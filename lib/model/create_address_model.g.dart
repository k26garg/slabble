// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_address_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateAddressModel _$CreateAddressModelFromJson(Map<String, dynamic> json) {
  return CreateAddressModel(
      json['addressText'] as String,
      json['city'] as String,
      json['pincode'] as String,
      json['state'] as String,
      json['flatNo'] as String,
      json['landmark'] as String,
      json['latitude'] as String,
      json['longitude'] as String,
      json['type'] as String);
}

Map<String, dynamic> _$CreateAddressModelToJson(CreateAddressModel instance) =>
    <String, dynamic>{
      'addressText': instance.addressText,
      'city': instance.city,
      'flatNo': instance.flatNo,
      'landmark': instance.landmark,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'pincode': instance.pincode,
      'state': instance.state,
      'type': instance.type
    };
