import 'package:json_annotation/json_annotation.dart';

part 'services.g.dart';

@JsonSerializable()
class Services{

  int serviceId;
  String name;
  String image;


  Services(this.serviceId, this.name, this.image);

  factory Services.fromJson(Map<String, dynamic> json) => _$ServicesFromJson(json);


  Map<String, dynamic> toJson() => _$ServicesToJson(this);

}