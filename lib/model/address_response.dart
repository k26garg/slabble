import 'package:json_annotation/json_annotation.dart';

part 'address_response.g.dart';

@JsonSerializable()
class AddressResponse{

    int addressId;
    String text;
    String type;

    AddressResponse(this.addressId, this.text,this.type);

    factory AddressResponse.fromJson(Map<String, dynamic> json) => _$AddressResponseFromJson(json);


    Map<String, dynamic> toJson() => _$AddressResponseToJson(this);

}