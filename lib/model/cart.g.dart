// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cart _$CartFromJson(Map<String, dynamic> json) {
  return Cart(
      json['cartId'] as int,
      (json['items'] as List)
          ?.map((e) => e == null
              ? null
              : CartOrderItem.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$CartToJson(Cart instance) =>
    <String, dynamic>{'cartId': instance.cartId, 'items': instance.items};
