// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_location_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchLocationModel _$SearchLocationModelFromJson(Map<String, dynamic> json) {
  return SearchLocationModel(
      json['description'] as String,
      json['structured_formatting'] == null
          ? null
          : SearchLocationTextModel.fromJson(
              json['structured_formatting'] as Map<String, dynamic>))
    ..place_id = json['place_id'] as String;
}

Map<String, dynamic> _$SearchLocationModelToJson(
        SearchLocationModel instance) =>
    <String, dynamic>{
      'description': instance.description,
      'place_id': instance.place_id,
      'structured_formatting': instance.structured_formatting
    };
