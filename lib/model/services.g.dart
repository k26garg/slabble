// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'services.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Services _$ServicesFromJson(Map<String, dynamic> json) {
  return Services(json['serviceId'] as int, json['name'] as String,
      json['image'] as String);
}

Map<String, dynamic> _$ServicesToJson(Services instance) => <String, dynamic>{
      'serviceId': instance.serviceId,
      'name': instance.name,
      'image': instance.image
    };
