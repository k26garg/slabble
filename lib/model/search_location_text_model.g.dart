// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_location_text_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchLocationTextModel _$SearchLocationTextModelFromJson(
    Map<String, dynamic> json) {
  return SearchLocationTextModel(
      json['main_text'] as String, json['secondary_text'] as String);
}

Map<String, dynamic> _$SearchLocationTextModelToJson(
        SearchLocationTextModel instance) =>
    <String, dynamic>{
      'main_text': instance.main_text,
      'secondary_text': instance.secondary_text
    };
