import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:io';

import 'http_param_object.dart';

class HttpService {
  static Future<dynamic> getData(HttpParamObject param) async {


    if (param != null) {
      if ("POST".toLowerCase() == param.method) {
        String url = param.url;
        Map<String, String> headers = param.headers;
        Map<String, dynamic> body = param.postParams;

        HttpClient client = new HttpClient();

        client.getUrl(Uri.parse(url));

        http.Response response = await http
            .post(url, body: json.encode(body), headers: headers)
            .timeout(const Duration(seconds: 30), onTimeout: () {
          throw 'Session Timeout !!';
        });

        if (response.statusCode >= 500) {
          throw 'Something went wrong !!';
        } else {
          var convert = JsonDecoder().convert(response.body);
          return convert;
        }
      }

      if ("PUT".toLowerCase() == param.method) {
        String url = param.url;
        Map<String, String> headers = param.headers;
        Map<String, dynamic> body = param.postParams;

        HttpClient client = new HttpClient();

        client.getUrl(Uri.parse(url));

        http.Response response = await http
            .put(url, body: json.encode(body), headers: headers)
            .timeout(const Duration(seconds: 30), onTimeout: () {
          throw 'Session Timeout !!';
        });

        if (response.statusCode >= 500) {
          throw 'Something went wrong !!';
        } else {
          var convert = JsonDecoder().convert(response.body);
          return convert;
        }
      }

      if ("GET".toLowerCase() == param.method) {
        String url = param.url;
        Map<String, String> headers = param.headers;

        HttpClient client = new HttpClient();

        client.getUrl(Uri.parse(url));

        http.Response response = await http
            .get(
          url,
          headers: headers,
        )
            .timeout(const Duration(seconds: 30), onTimeout: () {


              throw 'Session Timeout !!';
        });

        if (response.statusCode >= 500) {
          throw 'Something went wrong !!';
        } else {
          var convert;
          try {
            convert = JsonDecoder().convert(response.body);
          } catch (exception) {
            convert = exception.source;
          }
          return convert;
        }
      }
    }
  }
}
