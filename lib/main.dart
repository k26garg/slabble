import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:slabble/ui/account/enter_email.dart';
import 'package:slabble/ui/account/enter_name.dart';
import 'package:slabble/ui/splash_screen.dart';
import 'common/constants.dart';
import 'package:slabble/ui/account/otp_ui.dart';
import 'common/preferences.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  Preferences.initPreference();
  runApp(
    new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      theme: buildSlabbleTheme(),
    ),
  );
}

