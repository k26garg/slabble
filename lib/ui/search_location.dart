import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:slabble/blocs/address_bloc.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/search_location_bloc.dart';
import 'package:slabble/model/address_response.dart';
import 'package:slabble/model/location_result.dart';
import 'package:slabble/model/search_location_model.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/common/constants.dart';

import 'create-address.dart';

class SearchLocation extends StatefulWidget {
  String addressType;
  SearchLocation(this.addressType);

  @override
  _SearchLocationState createState() => _SearchLocationState();
}

class _SearchLocationState extends State<SearchLocation> {
  SearchLocationBloc searchLocationBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  GoogleMapsPlaces _places;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    searchLocationBloc = BlocProvider.of<SearchLocationBloc>(context);
    _places =
        GoogleMapsPlaces(apiKey: "AIzaSyAyjW7yBPc3ZJXOxaCd2SpCvKxaIpooQHg");
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          iconTheme: IconThemeData(color: Colors.grey),
        ),
        body: new Container(
            padding:
                EdgeInsets.only(left: 16.0, right: 8.0, top: 8.0, bottom: 8.0),
            child: new ListView.builder(
              itemCount: 5,
              itemBuilder: (context, index) {
                if (index == 0) {
                  return new Container(
                    child: new Text("Search for pickup location"),
                  );
                }

                if (index == 1) {
                  return new Container(
                    child: TextField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Enter location",
                            hintStyle:
                                customRegularTextStyle(color: Colors.grey)),
                        onChanged: (value) {
                          searchLocationBloc.getLocationFromSearch(value);
                        }),
                    padding: EdgeInsets.only(right: 8.0, top: 4.0, bottom: 4.0),
                  );
                }

                if (index == 2) {
                  return new StreamBuilder(
                    stream: searchLocationBloc.getLocationResult,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.active &&
                          snapshot.data != null) {
                        List<SearchLocationModel> locationResult =
                            snapshot.data;

                        return ListView.builder(
                          itemCount: locationResult.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return new InkWell(
                              child: new Container(
                                  margin: EdgeInsets.only(top: 12.0),
                                  child: new Row(
                                    children: <Widget>[
                                      new Icon(
                                        Icons.location_on,
                                        color: CustomColors.darkGrey,
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(left: 12.0),
                                      ),
                                      new Expanded(
                                        child: new Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            new Text(
                                              locationResult[index]
                                                  .structured_formatting
                                                  .main_text,
                                              style: customRegularTextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14.0),
                                            ),
                                            new Container(
                                              margin: EdgeInsets.only(top: 2.0),
                                            ),
                                            new Text(
                                              locationResult[index].description,
                                              style: customRegularTextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 14.0),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  )),
                              onTap: () {
                                displayPrediction(
                                    locationResult[index].place_id,
                                    locationResult[index].description);
                              },
                              splashColor: CustomColors.mediumDarkGrey,
                            );
                          },
                        );
                      } else {
                        return new Center(
                          child: Container(),
                        );
                      }
                    },
                  );
                }

                if (index == 3) {
                  return new Container(
                    margin: EdgeInsets.only(top: 12.0),
                    child: new Text(
                      "Saved address",
                      style: customMediumTextStyle(
                          color: CustomColors.purple, fontSize: 14.0),
                    ),
                  );
                }

                if (index == 4) {
                  return new StreamBuilder(
                    stream: searchLocationBloc.getAddresses,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        List response = snapshot.data["addresses"];

                        if (response != null && response.length > 0) {

                          List<AddressResponse> addressList = response
                              .map((m) => AddressResponse.fromJson(m))
                              .toList();

                          return new Container(
                            child: new ListView.builder(
                              shrinkWrap: true,
                              itemCount: addressList.length,
                              itemBuilder: (context, index) {
                                return new Container(
                                  margin: EdgeInsets.only(top: 12.0),
                                  child: new GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context,addressList[index].text);
                                    },
                                    child: new Row(
                                      children: <Widget>[
                                        new Icon(
                                          Icons.my_location,
                                          color: Colors.grey,
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(left: 12.0),
                                        ),
                                        new Expanded(
                                          child: new Text(
                                            addressList[index].text,
                                            maxLines: 3,
                                            style: customRegularTextStyle(
                                                fontSize: 14.0,
                                                color: Colors.black),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          );
                        } else {
                          return new Container();
                        }
                      } else {
                        return new Container();
                      }
                    },
                  );
                }
              },
            )));
  }

  Future<Null> displayPrediction(String placeId, String description) async {
    if (placeId != null) {
      LocationResult locationResult = new LocationResult();

      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(placeId);

      String lat = detail.result.geometry.location.lat.toString();
      String lng = detail.result.geometry.location.lng.toString();

      detail.result.addressComponents
          .map((m) => {
                if (m.types.contains("postal_code"))
                  {
                    locationResult.postalCode = m.longName,
                    description = description.replaceAll(m.longName, "")
                  },
                if (m.types.contains("country"))
                  {
                    locationResult.country = m.longName,
                    description = description.replaceAll(m.longName, "")
                  },
                if (m.types.contains("administrative_area_level_1"))
                  {
                    locationResult.state = m.longName,
                    description = description.replaceAll(m.longName, "")
                  },
                if (m.types.contains("locality"))
                  {
                    locationResult.city = m.longName,
                    description = description.replaceAll(m.longName, "")
                  }
              })
          .toList();

      print("");
      description = description.replaceAll(" ,", "");
      locationResult.description = description;
      locationResult.latitude = lat;
      locationResult.longitude = lng;
      print("");

      Navigator.of(context)
          .push(MaterialPageRoute(builder: (BuildContext context) {
        return BlocProvider<AddressBloc>(
          bloc: AddressBloc(),
          child: CreateAddressUI(locationResult,widget.addressType),
        );
      }));
    }
  }
}
