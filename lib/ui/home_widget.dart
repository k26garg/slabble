import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/home_bloc.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/ui/user_profile.dart';

import 'home_screen.dart';

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  int currentIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: getWidgetOnTap(currentIndex),
      bottomNavigationBar: new BottomNavigationBar(
        onTap: (value) {
          setState(() {
            currentIndex = value;
          });
        },

        backgroundColor: Colors.white,
        currentIndex: currentIndex,
        showUnselectedLabels: true,
        elevation: 10.0,
        selectedItemColor: CustomColors.darkYellow,
        unselectedItemColor: Colors.grey,
        type: BottomNavigationBarType.fixed,
        unselectedLabelStyle: customRegularTextStyle(fontSize: 12.0),
        selectedLabelStyle: customRegularTextStyle(fontSize: 12.0),
        items: [
          BottomNavigationBarItem(
            title: new Text('Home'),
            icon: Image.asset(
              "icons/home_outline.png",
              width: IconTheme.of(context).size,
              height: IconTheme.of(context).size,
              color: Colors.grey,
            ),
            activeIcon: Image.asset(
              "icons/home_purple.png",
              width: IconTheme.of(context).size,
              height: IconTheme.of(context).size,
              color: CustomColors.darkYellow,
            ),
          ),
          BottomNavigationBarItem(
            title: new Text(
              'My Package',
            ),
            icon: Image.asset(
              "icons/service_outline.png",
              width: IconTheme.of(context).size,
              height: IconTheme.of(context).size,
              color: Colors.grey,
            ),
            activeIcon: Image.asset(
              "icons/service_black.png",
              width: IconTheme.of(context).size,
              height: IconTheme.of(context).size,
              color: CustomColors.darkYellow,
            ),
          ),
          BottomNavigationBarItem(
            title: new Text('Help'),
            icon: Image.asset(
              "icons/help_outline.png",
              width: IconTheme.of(context).size,
              height: IconTheme.of(context).size,
              color: Colors.grey,
            ),
            activeIcon: Image.asset(
              "icons/help_purple.png",
              width: IconTheme.of(context).size,
              height: IconTheme.of(context).size,
              color: CustomColors.darkYellow,
            ),
          ),
          BottomNavigationBarItem(
            title: new Text('Profile'),
            icon: Image.asset(
              "icons/avatar_outline.png",
              width: IconTheme.of(context).size,
              height: IconTheme.of(context).size,
              color: Colors.grey,
            ),
            activeIcon: Image.asset(
              "icons/avatar_black.png",
              width: IconTheme.of(context).size,
              height: IconTheme.of(context).size,
              color: CustomColors.darkYellow,
            ),
          ),
        ],
      ),
    );
  }

  Widget getWidgetOnTap(currentIndex) {
    HapticFeedback.vibrate();
    if (currentIndex == 0) {
      return new BlocProvider<HomeBloc>(
        bloc: HomeBloc(),
        child: HomeScreen(),
      );
    } else if (currentIndex == 1) {
      return HomeScreen();
    } else if (currentIndex == 2) {
      return HomeScreen();
    } else if (currentIndex == 3) {
      return UserProfile();
    }
  }
}
