import 'dart:io';

import 'package:flutter/material.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/order_bloc.dart';
import 'package:slabble/blocs/service_bloc.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/model/cart.dart';
import 'package:slabble/model/packages.dart';
import 'package:slabble/model/services.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/request/create_cart_request.dart';

import 'order_service.dart';

class ServiceScreen extends StatefulWidget {
  Packages packages;

  ServiceScreen(this.packages);

  @override
  _ServiceScreenState createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen> {
  ServiceBloc serviceBloc;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    serviceBloc = BlocProvider.of<ServiceBloc>(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            widget.packages.name,
            style: customRegularTextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          leading: Icon(Icons.arrow_back,color: Colors.grey,),
        ),
        body: new StreamBuilder(
          stream: serviceBloc.getServiceList,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.data != null) {
              List responseList = snapshot.data["services"];
              List<Services> services =
                  responseList.map((m) => new Services.fromJson(m)).toList();


              return ListView.builder(
                itemCount: services.length + 1,
                itemBuilder: (context, index) {
                  if (index < services.length) {
                    return new Card(
                      child: new InkWell(
                        onTap: () {},
                        child: new Container(
                            padding: EdgeInsets.all(16.0),
                            child: new Row(
                              children: <Widget>[
                                new Image.network(
                                  services[index].image,
                                  width: 32.0,
                                  height: 32.0,
                                ),
                                new Container(
                                  margin: EdgeInsets.only(left: 12.0),
                                ),
                                new Text(
                                  services[index].name,
                                  style: customRegularTextStyle(),
                                ),
                                new Expanded(
                                    child: new Container(
                                  child: new Icon(
                                    Icons.arrow_forward_ios,
                                    size: 12.0,
                                  ),
                                  alignment: Alignment.centerRight,
                                ))
                              ],
                            )),
                      ),
                    );
                  }
                  if (index == services.length) {
                    return new Card(
                      child: new InkWell(
                        onTap: () {},
                        child: new Container(
                            padding: EdgeInsets.all(16.0),
                            child: new Row(
                              children: <Widget>[
                                new Icon(
                                  Icons.add,
                                ),
                                new Container(
                                  margin: EdgeInsets.only(left: 12.0),
                                ),
                                new Text(
                                  "Add More Services",
                                  style: customRegularTextStyle(),
                                ),
                              ],
                            )),
                      ),
                    );
                  }
                },
              );
            } else {
              return new Container();
            }
          },
        ),
        bottomNavigationBar: new StreamBuilder(
          stream: serviceBloc.getLoader,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.active &&
                !snapshot.data) {
              return new Container(
                child: new RaisedButton(
                  onPressed: ()  {
                     addToCart(widget.packages.packageId,
                        widget.packages.minBathRomCount, widget.packages.minBedRoomCount);

                  },
                  color: CustomColors.darkYellow,
                  child: new Text(
                    "Add To Cart | " + formatCurrency.format(widget.packages.price).toString(),
                    style: customRegularTextStyle(color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  padding: EdgeInsets.all(14.0),
                ),
                margin: EdgeInsets.only(left: 22.0, right: 22.0, bottom: 22.0),
              );
            } else {
              return new Container(
                child: new CircularProgressIndicator(),
              );
            }
          },
        ));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    serviceBloc.dispose();
    super.dispose();
  }

  addToCart(int packageId, int minBathRomCount, int minBedRoomCount) async {
    serviceBloc.showLoader.sink.add(true);
    CreateCartRequest cartRequest =
        new CreateCartRequest(packageId, minBathRomCount, minBedRoomCount);
    dynamic response = await ApiRequest.allPostApi(
            url: "api/v1/user/update-cart",
            postParams: cartRequest.toJson(),
            headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .then((value) {
      Cart cart = Cart.fromJson(value);
      serviceBloc.showLoader.sink.add(false);
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (BuildContext context) {
        return BlocProvider<OrderBloc>(
          bloc: OrderBloc(),
          child: OrderService(cart),
        );
      }));
    });

    return response;
  }
}
