import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/home_bloc.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/common/preferences.dart';
import 'package:slabble/request/sign_up_request.dart';
import 'package:slabble/response/base_response.dart';
import 'package:http/http.dart' as http;
import '../home_screen.dart';

class EnterEmail extends StatefulWidget {
  String firstName;
  String lastName;
  String mobileNumber;

  EnterEmail(this.firstName, this.lastName, this.mobileNumber);

  @override
  _EnterEmailState createState() => _EnterEmailState();
}

class _EnterEmailState extends State<EnterEmail> {
  String email;
  bool showLoader = false;

  GoogleSignInAccount googleAccount;
  final GoogleSignIn googleSignIn = new GoogleSignIn();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: new Builder(builder: (BuildContext context) {
        return new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              child: new Text(
                "Sign Up",
                style: customMediumBoldTextStyle(fontSize: 24),
              ),
              padding: EdgeInsets.all(32.0),
            ),
            /*showLoader
                ? new Container(
              child:new SpinKitRing(
                color: Color(0xFFFFD400),
                lineWidth: 6.0,
              ),
              alignment: Alignment.center,
              color: CustomColors.lightBlack,
            )
                : new Container()*/
            facebookLoginButton(),
            googleLoginButton(),
          ],
        );
      }),
    );
  }

  Widget facebookLoginButton() {
    return new Container(
      child: FlatButton(
        onPressed: () {
          initFacebookLogin().then((value) async {
            FirebaseUser firebaseUser = value;

            SignUpRequest request = new SignUpRequest(
                firebaseUser.displayName, firebaseUser.email, widget.mobileNumber,firebaseUser.photoUrl,'FACEBOOK');
            dynamic json = await saveCustomerInfo(request);
            BaseResponse response = BaseResponse.fromJson(json);
            if(response.statusCode==201){
              Preferences.saveData(Preferences.KEY_AUTH_TOKEN, response.token);
              setState(() {
                showLoader = false;
              });

              /*FreshchatUser user = FreshchatUser.initail();
              user.email = parseJwt()["email"];
              user.firstName = parseJwt()["name"];
              user.phoneCountryCode = "+91";
              user.phone = parseJwt()["mobile"];

              await FlutterFreshchat.updateUserInfo(user: user);*/

// Custom properties can be set by creating a Map<String, String>
              /* Map<String, String> customProperties = Map<String, String>();
              customProperties["loggedIn"] = "true";

              await FlutterFreshchat.updateUserInfo(user: user, customProperties: customProperties);*/
              Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
                return BlocProvider<HomeBloc>(
                  bloc: HomeBloc(),
                  child: HomeScreen(),
                );
              }));
            }
          });
        },
        splashColor: Color.fromARGB(40, 255, 255, 255),
        color: Color(0xff1e599b),
        child: Row(
          children: <Widget>[
            new Container(
              child: SvgPicture.asset('icons/icon_facebook.svg', height: 24.0),
              padding: EdgeInsets.only(right: 8.0),
            ),
            new Text(
              "Connect with Facebook",
              style: TextStyle(color: Colors.white, fontSize: 16.0),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
        padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
      ),
      padding: EdgeInsets.only(left: 32.0, right: 32.0),
      margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
    );
  }

  Widget googleLoginButton() {
    return new Container(
      child: OutlineButton(
        onPressed: () {
          initiateGoogleLogin().then((value) async {
            FirebaseUser firebaseUser = value;

            SignUpRequest request = new SignUpRequest(
                firebaseUser.displayName, firebaseUser.email, widget.mobileNumber,firebaseUser.photoUrl,'GOOGLE');
            dynamic json = await saveCustomerInfo(request);
            BaseResponse response = BaseResponse.fromJson(json);
            if(response.statusCode==201){
              Preferences.saveData(Preferences.KEY_AUTH_TOKEN, response.token);
              setState(() {
                showLoader = false;
              });

              /*FreshchatUser user = FreshchatUser.initail();
              user.email = parseJwt()["email"];
              user.firstName = parseJwt()["name"];
              user.phoneCountryCode = "+91";
              user.phone = parseJwt()["mobile"];

              await FlutterFreshchat.updateUserInfo(user: user);*/

// Custom properties can be set by creating a Map<String, String>
             /* Map<String, String> customProperties = Map<String, String>();
              customProperties["loggedIn"] = "true";

              await FlutterFreshchat.updateUserInfo(user: user, customProperties: customProperties);*/
              Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
                return BlocProvider<HomeBloc>(
                  bloc: HomeBloc(),
                  child: HomeScreen(),
                );
              }));
            }



          });
        },
        splashColor: Color.fromARGB(40, 255, 255, 255),
        color: Color(0xffffffff),
        child: Row(
          children: <Widget>[
            new Container(
              child: SvgPicture.asset('icons/icon_google.svg', height: 24.0),
              padding: EdgeInsets.only(right: 8.0),
            ),
            new Text(
              "Connect with Google",
              style: TextStyle(color: Color(0xff444444), fontSize: 16.0),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ),
        borderSide: BorderSide(color: Color(0xffd8d8d8), width: 1.0),
        highlightedBorderColor: Color(0xffd8d8d8),
        highlightColor: Colors.transparent,
        highlightElevation: 0,
        padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
      ),
      padding: EdgeInsets.only(left: 32.0, right: 32.0),
      margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
    );
  }

  saveCustomerInfo(SignUpRequest request) async {

    dynamic response = await ApiRequest.allPostApi(
        url: "api/v1/auth/signup?mobileNumber=" + widget.mobileNumber,
        postParams: request.toJson(),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'});

    return response;
  }

  Future<FirebaseUser> initFacebookLogin() async{

    final facebookLogin = FacebookLogin();


    FacebookLoginResult facebookLoginResult = await facebookLogin.logIn(['email']);

    AuthCredential credential = FacebookAuthProvider.getCredential(accessToken: facebookLoginResult.accessToken.token);

    FirebaseAuth _auth = FirebaseAuth.instance;

    final graphResponse = await http.get(
        'https://graph.facebook.com/v2.12/me?fields=picture.width(200).height(200)&access_token=${facebookLoginResult.accessToken.token}');
    final profile = json.decode(graphResponse.body);

    FirebaseUser user = (await _auth.signInWithCredential(credential)).user;


    return user;

  }


  Future<FirebaseUser> initiateGoogleLogin() async {
    googleAccount = await googleSignIn.signIn();
    return await signIntoFirebase(googleAccount);

  }

  Future<FirebaseUser> signIntoFirebase(
      GoogleSignInAccount googleSignInAccount) async {

    FirebaseAuth _auth = FirebaseAuth.instance;
    final GoogleSignInAuthentication googleAuth = await googleSignInAccount.authentication;


    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;

    return user;
  }
}
