import 'dart:io';

import 'package:flutter/material.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/common/enter_exit_route.dart';

import 'otp_verify.dart';

class OtpUi extends StatefulWidget{
  @override
  _OtpUiState createState() => _OtpUiState();
}

class _OtpUiState extends State<OtpUi> {

  String mobileNumber;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(

      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: new Builder(
        builder: (BuildContext context) {
          return new Stack(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.all(32.0),
                child: new Container(

                    child: new ListView.builder(itemBuilder: (context, index) {

                      if(index==0){
                        return new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text("Let's start",style: customMediumBoldTextStyle(fontSize: 24),),
                          ],
                        );
                      }

                      if (index == 1) {
                        return new Container(
                          margin: EdgeInsets.only(top: 46.0),
                          child: Image.asset(
                            "icons/mobile-sms.png",
                            width: 92,
                            height: 96,
                          ),
                        );
                      }

                      if (index == 2) {
                        return new Container(
                          child: new Text(
                            "Verify Your Phone Number",
                            style: customMediumTextStyle(),
                            textAlign: TextAlign.center,
                          ),
                          margin: EdgeInsets.only(top: 16.0),
                        );
                      }

                      if (index == 3) {
                        return new Container(
                          child: new Text(
                            "Enter your mobile number we will send, you OTP to verify later",
                            textAlign: TextAlign.center,
                            style: customRegularTextStyle(lineHeight: 1.2),
                          ),
                          margin: EdgeInsets.only(top: 16.0),
                        );
                      }

                      if (index == 4) {
                        return  Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                color: CustomColors.mediumDarkGrey,
                              ),
                              borderRadius: BorderRadius.circular(8.0)),
                          padding: EdgeInsets.all(2.0),
                          margin: EdgeInsets.only(
                              top: 64.0, left: 8.0, right: 8.0),
                          child: new Row(
                            children: <Widget>[
                              new Container(
                                child: new Image.asset(
                                  "icons/india.png",
                                  width: 26.0,
                                  height: 26.0,
                                ),
                                margin: EdgeInsets.only(left: 8.0),
                              ),
                              new Container(
                                child: new Text("(+91)",style: customRegularTextStyle(color: CustomColors.mediumDarkGrey),),
                                margin: EdgeInsets.only(left: 8.0),
                              ),
                              new Container(
                                width: 1.0,
                                height: 20,
                                color: CustomColors.mediumDarkGrey,
                                margin: EdgeInsets.only(left: 8.0),
                              ),
                              new Expanded(
                                child: new Container(
                                  child: new TextField(
                                    style: customMediumTextStyle(),
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: "Phone Number",
                                      hintStyle: customRegularTextStyle(color: CustomColors.mediumDarkGrey),
                                      counterText: "",
                                    ),
                                    maxLength: 10,
                                    maxLengthEnforced: true,
                                    onChanged: (value){
                                      setState(() {
                                        mobileNumber = value;
                                      });
                                    },
                                  ),
                                  margin: EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                ),
                              ),
                            ],
                          ),
                        );
                      }

                      if (index == 5) {
                        return new  Container(

                          decoration: BoxDecoration(
                            gradient: CustomColors.yellowGradientForText,
                            boxShadow: [
                              new BoxShadow(
                                  color: Colors.grey.withOpacity(0.25),
                                  offset: new Offset(0, 4.0),
                                  blurRadius: 4.0,
                                  spreadRadius: 0.0)
                            ],
                          ),
                          margin: EdgeInsets.only(top: 64),
                          height: 48,
                          child: MaterialButton(
                            onPressed: ()  async {
                              if (mobileNumber!= null && mobileNumber.length==10) {
                                dynamic response = await getOTPApi(mobileNumber);
                                bool isSend = response;

                                if(isSend){
                                  Navigator.push(
                                      context,
                                      EnterExitRoute(
                                          exitPage: OtpUi(),
                                          enterPage: OTPVerify(mobileNumber)));
                                }

                              }
                            },
                            splashColor: Color.fromARGB(40, 255, 255, 255),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Verify",
                                  style: customMediumTextStyle(color: Colors.black,fontSize: 18.0),
                                  textAlign: TextAlign.center,
                                  softWrap: true,
                                ),
                                Container(
                                  child: Icon(Icons.arrow_forward,color: Colors.black,),
                                  margin: EdgeInsets.only(left: 4.0),
                                )
                              ],
                            ),
                          ),
                          width: double.infinity,
                        );;
                      }




                    })),
              )

            ],
          );
        },
      ),
    );
  }

  getOTPApi(String phoneNumber) async{
    bool response = await ApiRequest.allGetApi(
        url: "api/v1/auth/generateotp/$phoneNumber",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'}
    );

    return response;
  }
}