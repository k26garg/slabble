import 'dart:io';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/common/constants.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/common/enter_exit_route.dart';
import 'package:slabble/request/sign_up_request.dart';
import 'package:slabble/response/base_response.dart';
import 'package:slabble/ui/account/enter_name.dart';

import 'enter_email.dart';

class OTPVerify extends StatefulWidget {
  String mobileNumber;

  OTPVerify(this.mobileNumber);

  @override
  _OTPVerifyState createState() => _OTPVerifyState();
}

class _OTPVerifyState extends State<OTPVerify> {
  String code;
  bool showLoader = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: new Builder(
        builder: (BuildContext context) {
          return new Stack(
            children: <Widget>[
              new Container(
                  padding: EdgeInsets.all(32.0),
                  child: new Container(child:
                      new ListView.builder(itemBuilder: (context, index) {
                    if (index == 0) {
                      return new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "OTP Verify",
                            style: customMediumBoldTextStyle(fontSize: 24),
                          ),
                        ],
                      );
                    }

                    if (index == 1) {
                      return new Container(
                        margin: EdgeInsets.only(top: 46.0),
                        child: Image.asset(
                          "icons/mobile-sms.png",
                          width: 92,
                          height: 96,
                        ),
                      );
                    }

                    if (index == 2) {
                      return new Container(
                        child: new Text(
                          "Verification Code",
                          style: customMediumTextStyle(),
                          textAlign: TextAlign.center,
                        ),
                        margin: EdgeInsets.only(top: 16.0),
                      );
                    }

                    if (index == 3) {
                      return new Container(
                        child: new Text(
                          "Enter 4 digits that sent to \n ${widget.mobileNumber} ",
                          textAlign: TextAlign.center,
                          style: customRegularTextStyle(lineHeight: 1.2),
                        ),
                        margin: EdgeInsets.only(top: 16.0),
                      );
                    }

                    if (index == 4) {
                      return new Container(
                        margin: EdgeInsets.only(top: 64.0),
                        child: PinCodeTextField(
                          maxLength: 4,
                          isCupertino: true,
                          pinBoxWidth: 50.0,
                          pinBoxHeight: 50.0,
                          defaultBorderColor: Colors.grey,
                          autofocus: false,
                          pinCodeTextFieldLayoutType:
                              PinCodeTextFieldLayoutType.AUTO_ADJUST_WIDTH,
                          wrapAlignment: WrapAlignment.start,
                          pinBoxDecoration:
                              ProvidedPinBoxDecoration.defaultPinBoxDecoration,
                          pinTextStyle: TextStyle(fontSize: 22.0),
                          pinTextAnimatedSwitcherTransition:
                              ProvidedPinBoxTextAnimation.scalingTransition,
                          pinTextAnimatedSwitcherDuration:
                              Duration(milliseconds: 300),
                          onDone: (value) {
                            setState(() {
                              code = value;
                            });
                          },
                        ),
                      );
                    }

                    if (index == 5) {
                      return new Container(
                        decoration: BoxDecoration(
                          gradient: CustomColors.yellowGradientForText,
                          boxShadow: [
                            new BoxShadow(
                                color: Colors.grey.withOpacity(0.25),
                                offset: new Offset(0, 4.0),
                                blurRadius: 4.0,
                                spreadRadius: 0.0)
                          ],
                        ),
                        margin: EdgeInsets.only(top: 64),
                        height: 48,
                        child: MaterialButton(
                          onPressed: () async {
                            setState(() {
                              showLoader = true;
                            });
                            dynamic response = await verifyOTP();
                            bool isVerify = response;

                            if (isVerify) {

                              dynamic response = await registerCustomer();
                              BaseResponse baseResponse = BaseResponse.fromJson(response);
                              setState(() {
                                showLoader = false;
                              });
                              if(baseResponse.statusCode==201){
                                Navigator.pushReplacement(
                                    context,
                                    EnterExitRoute(
                                        exitPage: EnterName(widget.mobileNumber),
                                        enterPage: EnterEmail(null,null,widget.mobileNumber)));
                              }
                              else{
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content: Text("Failed"),
                                    duration: Duration(seconds: 5)));
                              }

                            } else {
                              setState(() {
                                showLoader = false;
                              });
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text("Failed"),
                                  duration: Duration(seconds: 5)));
                            }
                          },
                          splashColor: Color.fromARGB(40, 255, 255, 255),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Submit",
                                style: customMediumTextStyle(
                                    color: Colors.black, fontSize: 18.0),
                                textAlign: TextAlign.center,
                                softWrap: true,
                              ),
                              Container(
                                child: Icon(
                                  Icons.arrow_forward,
                                  color: Colors.black,
                                ),
                                margin: EdgeInsets.only(left: 4.0),
                              )
                            ],
                          ),
                        ),
                        width: double.infinity,
                      );
                      ;
                    }
                  }))),
              showLoader
                  ? new Container(
                child:new SpinKitRing(
                  color: Color(0xFFFFD400),
                  lineWidth: 6.0,
                ),
                alignment: Alignment.center,
                color: CustomColors.lightBlack,
              )
                  : new Container()
            ],
          );
        },
      ),
    );
  }

  verifyOTP() async {
    bool response = await ApiRequest.allGetApi(
        url: "api/v1/auth/validateotp/${widget.mobileNumber}/$code",
        headers: {HttpHeaders.contentTypeHeader: 'application/json'});

    return response;
  }

  registerCustomer() async{
    SignUpRequest request = new SignUpRequest(null, null, widget.mobileNumber, null,null);
    dynamic response = await ApiRequest.allPostApi(
      url: "api/v1/auth/signup",
      postParams: request.toJson(),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'}
    );

    return response;
  }
}
