import 'package:flutter/material.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/common/enter_exit_route.dart';
import 'package:slabble/ui/account/enter_email.dart';

class EnterName extends StatefulWidget {
  String mobileNumber;

  EnterName(this.mobileNumber);

  @override
  _EnterNameState createState() => _EnterNameState();
}

class _EnterNameState extends State<EnterName> {
  String firstName;

  String lastName;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: new Builder(builder: (BuildContext context) {
        return new Container(
          padding: EdgeInsets.all(32.0),
          child: Center(
            child: new Container(
              child: new ListView.builder(itemBuilder: (context, index) {

                if(index==0){
                  return new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text("Enter Name",style: customMediumBoldTextStyle(fontSize: 24),),

                    ],
                  );
                }

                if(index==1){
                  return new Container(
                    margin: EdgeInsets.only(top: 46),
                    child: new TextField(
                      style: customMediumTextStyle(),
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "first name",
                        hintStyle: customRegularTextStyle(color: CustomColors.mediumDarkGrey),
                        counterText: "",
                      ),
                      onChanged: (value){
                          setState(() {
                            firstName = value;
                          });
                      },
                    ),
                  );
                }

                if(index==2){
                  return new Container(
                    margin: EdgeInsets.only(top: 16),
                    child: new TextField(
                      style: customMediumTextStyle(),
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "last name",
                        hintStyle: customRegularTextStyle(color: CustomColors.mediumDarkGrey),
                        counterText: "",
                      ),

                      onChanged: (value){
                        setState(() {
                          lastName = value;
                        });
                      },
                    ),
                  );
                }

                if (index == 3) {
                  return new  Container(

                    decoration: BoxDecoration(
                      gradient: CustomColors.yellowGradientForText,
                      boxShadow: [
                        new BoxShadow(
                            color: Colors.grey.withOpacity(0.25),
                            offset: new Offset(0, 4.0),
                            blurRadius: 4.0,
                            spreadRadius: 0.0)
                      ],
                    ),
                    margin: EdgeInsets.only(top: 64),
                    height: 48,
                    child: MaterialButton(
                      onPressed: ()  {
                        Navigator.push(
                            context,
                            EnterExitRoute(
                                exitPage: EnterName(widget.mobileNumber),
                                enterPage: EnterEmail(firstName,lastName,widget.mobileNumber)));

                      },
                      splashColor: Color.fromARGB(40, 255, 255, 255),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Next",
                            style: customMediumTextStyle(color: Colors.black,fontSize: 18.0),
                            textAlign: TextAlign.center,
                            softWrap: true,
                          ),
                          Container(
                            child: Icon(Icons.arrow_forward,color: Colors.black,),
                            margin: EdgeInsets.only(left: 4.0),
                          )
                        ],
                      ),
                    ),
                    width: double.infinity,
                  );;
                }

              }),
            ),
          ),
        );
      }),
    );
  }


}
