import 'package:flutter/material.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/user_address_bloc.dart';
import 'package:slabble/model/address_response.dart';
import 'package:slabble/common/constants.dart';

class UserAddress extends StatefulWidget {
  @override
  _UserAddressState createState() => _UserAddressState();
}

class _UserAddressState extends State<UserAddress> {
  UserAddressBloc userAddressBloc;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    userAddressBloc = BlocProvider.of<UserAddressBloc>(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: new Text(
          "My Address",
          style: customMediumBoldTextStyle(color: Colors.black),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: new Container(
        child: StreamBuilder(
          stream: userAddressBloc.getAllAddress,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.data != null) {
              List response = snapshot.data["addresses"];

              if (response != null && response.length > 0) {
                List<AddressResponse> addressList =
                    response.map((m) => AddressResponse.fromJson(m)).toList();

                return new ListView(
                  children: addressList.map((AddressResponse address) {
                    return new Container(
                      padding:
                          EdgeInsets.only(top: 16.0, right: 16, left: 16.0),
                      child: new Row(
                        children: <Widget>[
                          new Image.asset(
                            "icons/home-address.png",
                            width: 32,
                            height: 32,
                          ),
                          new Container(
                            margin: EdgeInsets.only(left: 16.0),
                          ),
                          new Expanded(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(
                                  address.type,
                                  style:
                                      customRegularTextStyle(lineHeight: 1.15),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(top: 8.0),
                                ),
                                new Text(address.text,
                                    style: customRegularTextStyle(

                                        color: Colors.grey,
                                        fontSize: 14)),
                                new Container(
                                  height: 1,
                                  color: Colors.grey,
                                  margin: EdgeInsets.only(top: 16.0),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  }).toList(),
                );
              } else {
                return new Center(
                  child: new Text(
                    "No Address!!",
                    style: customRegularTextStyle(
                        fontSize: 16, color: Colors.black),
                  ),
                );
              }
            } else {
              return new Container();
            }
          },
        ),
      ),
    );
  }
}
