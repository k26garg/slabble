import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_freshchat/flutter_freshchat.dart';
import 'package:location/location.dart';
import 'package:slabble/common/preferences.dart';
import 'account/otp_ui.dart';
import 'home_widget.dart';
import 'package:slabble/common/custom_color.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    initPlatformState();
    getLocation();

    /*Timer(Duration(seconds: 2), () async {
      if (Preferences.getData(Preferences.KEY_AUTH_TOKEN) != null) {
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
          return HomeWidget();
        }));
      } else {
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
          return OtpUi();
        }));
      }
    });*/
  }

  Future<void> initPlatformState() async {
    await FlutterFreshchat.init(
        appID: "65a15bc7-2a40-4958-95a8-831f7882079c",
        appKey: "975755b1-48f1-42d3-9025-5173a8bd45ee");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      backgroundColor: CustomColors.lightGrey,
        body: new Container(
          alignment: Alignment.center,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset("images/welcome_1.png"),

            ],
          ),
        ));
  }

  Future<dynamic> getLocation() async {
    LocationData myLocation;
    String error;
    Location location = new Location();
    try {
      myLocation = await location.getLocation();
    } catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'please grant permission';
        print(error);
      }
      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'permission denied- please enable it from app settings';
        print(error);
      }
      myLocation = null;
    }
    print("");
  }
}
