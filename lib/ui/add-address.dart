import 'package:flutter/material.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/search_location_bloc.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/ui/search_location.dart';

class AddAddress extends StatefulWidget {
  @override
  _AddAddressState createState() => _AddAddressState();
}

class _AddAddressState extends State<AddAddress> {
  String pickupLocation;
  String deliveryLocation;

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    // TODO: implement build
    return new Scaffold(
      body: new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new InkWell(
              child: new Container(
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        child: new Column(
                      children: <Widget>[
                        new Stack(
                          children: <Widget>[
                            new Icon(
                              Icons.location_searching,
                              color: Colors.grey,
                              size: 32,
                            ),
                            pickupLocation != null
                                ? new Container(
                                    child: new Icon(
                                      Icons.my_location,
                                      color: CustomColors.green,
                                      size: 32,
                                    ),
                                  )
                                : new Container()
                          ],
                        ),
                        new Container(
                          color: Colors.grey,
                          margin: EdgeInsets.only(top: 2.0),
                          child: new CustomPaint(
                            painter: LineDashedPainter(maxLine: 8),
                          ),
                        ),
                      ],
                    )),
                    new Container(
                      margin: EdgeInsets.only(left: 12.0),
                    ),
                    new Expanded(
                      child: new TextField(
                        maxLines: pickupLocation == null ? 1 : 2,
                        controller: TextEditingController(
                            text: pickupLocation == null
                                ? "Search pickup location"
                                : pickupLocation),
                        enabled: false,
                        maxLengthEnforced: false,
                        readOnly: true,
                        style: customMediumTextStyle(
                            color: pickupLocation == null
                                ? Colors.grey
                                : Colors.black,
                            fontSize: 14.0),
                        decoration: InputDecoration(
                          labelText: "Pickup location",
                          suffixIcon: new Icon(
                            Icons.search,
                            size: 16,
                          ),
                          labelStyle: customRegularTextStyle(
                              color: Colors.grey, fontSize: 16.0),
                          disabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: CustomColors.purple),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 24.0),
              ),
              splashColor: CustomColors.mediumDarkGrey,
              onTap: () async {
                var result = await Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return BlocProvider<SearchLocationBloc>(
                    bloc: SearchLocationBloc(),
                    child: SearchLocation("pickup"),
                  );
                }));
                setState(() {
                  pickupLocation = result;
                });
              },
            ),
            new Container(
              height: 40.0,
              padding: EdgeInsets.only(left: 24.0, right: 24.0),
              margin: EdgeInsets.only(left: 16.0, top: 4.0),
              child: new CustomPaint(
                painter: LineDashedPainter(maxLine: 38),
              ),
            ),
            new InkWell(
              child: new Container(
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        child: new Column(
                      children: <Widget>[
                        new Container(
                          color: Colors.grey,
                          margin: EdgeInsets.only(top: 2.0),
                          height: 8,
                          child: new CustomPaint(
                            painter: LineDashedPainter(maxLine: 8),
                          ),
                        ),
                        new Stack(
                          children: <Widget>[
                            new Icon(
                              Icons.location_searching,
                              color: Colors.grey,
                              size: 32,
                            ),
                            deliveryLocation != null
                                ? new Container(
                                    child: new Icon(
                                      Icons.my_location,
                                      color: CustomColors.green,
                                      size: 32,
                                    ),
                                  )
                                : new Container()
                          ],
                        ),
                        new Container(
                          color: Colors.grey,
                          margin: EdgeInsets.only(top: 2.0),
                          child: new CustomPaint(
                            painter: LineDashedPainter(maxLine: 8),
                          ),
                        ),
                      ],
                    )),
                    new Container(
                      margin: EdgeInsets.only(left: 12.0),
                    ),
                    new Expanded(
                      child: new TextField(
                        maxLines: deliveryLocation == null ? 1 : 2,
                        controller: TextEditingController(
                            text: deliveryLocation == null
                                ? "Search delivery location"
                                : deliveryLocation),
                        enabled: false,
                        style: customMediumTextStyle(
                            color: deliveryLocation == null
                                ? Colors.grey
                                : Colors.black,
                            fontSize: 14.0),
                        decoration: InputDecoration(
                          suffixIcon: new Icon(
                            Icons.search,
                            size: 16,
                          ),
                          labelText: "Delivery location",
                          labelStyle: customRegularTextStyle(
                              color: deliveryLocation == null
                                  ? Colors.grey
                                  : Colors.black,
                              fontSize: 16.0),
                          disabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: CustomColors.purple),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0),
              ),
              splashColor: CustomColors.mediumDarkGrey,
              onTap: () async {
                var result = await Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return BlocProvider<SearchLocationBloc>(
                    bloc: SearchLocationBloc(),
                    child: SearchLocation("delivery"),
                  );
                }));
                setState(() {
                  deliveryLocation = result;
                });
              },
            ),
          ],
        ),
      ),
      appBar: PreferredSize(
        child: new AppBar(
          elevation: 1.0,
          leading: new Container(),
          primary: true,
          flexibleSpace: new Container(
            margin: EdgeInsets.only(top: statusBarHeight),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new InkWell(
                  child: new Icon(
                    Icons.arrow_back,
                    color: Colors.grey,
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                new Container(
                  margin: EdgeInsets.only(top: 12.0),
                ),
                new Text(
                  "Enter Address",
                  style: customRegularTextStyle(),
                ),
              ],
            ),
            padding: EdgeInsets.only(left: 12.0),
          ),
        ),
        preferredSize: Size.fromHeight(80),
      ),
      bottomNavigationBar: new Container(
        child: new RaisedButton(
          onPressed: pickupLocation != null && deliveryLocation != null
              ? () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) {
                        return AddAddress();
                      },
                      fullscreenDialog: true));
                }
              : null,
          color: CustomColors.darkYellow,
          child: new Text("Confirm an order",
              style: customRegularTextStyle(color: Colors.black)),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          padding: EdgeInsets.all(14.0),
        ),
        margin: EdgeInsets.only(left: 22.0, right: 22.0, bottom: 22.0),
      ),
    );
  }
}

class LineDashedPainter extends CustomPainter {
  int maxLine;

  LineDashedPainter({this.maxLine});

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 2
      ..color = Colors.grey;
    var max = maxLine;
    var dashWidth = 4;
    var dashSpace = 4;
    double startY = 0;
    while (max >= 0) {
      canvas.drawLine(Offset(0, startY), Offset(0, startY + dashWidth), paint);
      final space = (dashSpace + dashWidth);
      startY += space;
      max -= space;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
