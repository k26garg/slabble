import 'package:flutter/material.dart';
import 'package:slabble/common/util.dart';
import 'package:slabble/common/constants.dart';

typedef void DateCallback(DateTime val);

class G2xSimpleWeekCalendar extends StatefulWidget {
  G2xSimpleWeekCalendar(this.currentDate,
      {this.strWeekDays = const [
        "Sun",
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri",
        "Sat"
      ],
      this.dateCallback,
      this.defaultTextStyle = const TextStyle(),
      this.selectedTextStyle = const TextStyle(color: Colors.red),
      this.selectedBackgroundDecoration = const BoxDecoration(),
      this.backgroundDecoration = const BoxDecoration(),
      this.unselectedBackgroundDecoration = const BoxDecoration()});

  final DateTime currentDate;
  final List<String> strWeekDays;
  final DateCallback dateCallback;

  //style
  final TextStyle defaultTextStyle;
  final TextStyle selectedTextStyle;
  final BoxDecoration selectedBackgroundDecoration;
  final BoxDecoration backgroundDecoration;
  final BoxDecoration unselectedBackgroundDecoration;

  @override
  _G2xSimpleWeekCalendarState createState() => _G2xSimpleWeekCalendarState();
}

class _G2xSimpleWeekCalendarState extends State<G2xSimpleWeekCalendar> {
  DateTime currentDate;
  var weekDays = <int>[];
  var selectedIndex = 0;

  _setSelectedDate(int index) {
    setState(() {
      selectedIndex = index;
      currentDate = MyDateTime.getFirstDateOfWeek(currentDate)
          .add(new Duration(days: index));
      if (widget.dateCallback != null) widget.dateCallback(currentDate);
    });
  }

  _altertWeek(int days) {
    setState(() {
      currentDate = currentDate.add(new Duration(days: days));
      if (widget.dateCallback != null) widget.dateCallback(currentDate);
    });
  }

  @override
  void initState() {
    super.initState();
    currentDate = widget.currentDate;
    if (widget.dateCallback != null) widget.dateCallback(currentDate);
    selectedIndex = currentDate.weekday == 7 ? 0 : currentDate.weekday;
  }
  @override
  Widget build(BuildContext context) {
    weekDays = MyDateTime.getDaysOfWeek(currentDate);

    var rowWeeks = new Column(
      children: <Widget>[
        new Row(
          children: <Widget>[
            new InkWell(
              onTap: () => _altertWeek(-7),
              splashColor: Colors.white,
              child: new Icon(Icons.arrow_left,
                  color: widget.defaultTextStyle.color),
            ),
            new InkWell(
              child: new Text(MyDateTime.formatDate(currentDate),
                  style: widget.defaultTextStyle),
              onTap: (){
                _selectDate(context);
              },
            ),
            new InkWell(
              onTap: () => _altertWeek(7),
              splashColor: Colors.white,
              child: new Icon(Icons.arrow_right,
                  color: widget.defaultTextStyle.color),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: widget.strWeekDays.map((i) {
              return new InkWell(
                  onTap: () => _setSelectedDate(widget.strWeekDays.indexOf(i)),
                  splashColor: Colors.white,
                  child: new Container(
                    child: new Column(
                      children: <Widget>[
                        weekDays[widget.strWeekDays.indexOf(i)] != 0
                            ? new Container(
                                child: new Text(i,
                                    style: selectedIndex ==
                                            widget.strWeekDays.indexOf(i)
                                        ? widget.selectedTextStyle
                                        : widget.defaultTextStyle),
                              )
                            : new Container(),
                        weekDays[widget.strWeekDays.indexOf(i)] != 0
                            ? new Container(
                                padding: EdgeInsets.all(8.0),
                                child: new Text(
                                    weekDays[widget.strWeekDays.indexOf(i)]
                                        .toString(),
                                    style: selectedIndex ==
                                            widget.strWeekDays.indexOf(i)
                                        ? widget.selectedTextStyle
                                        : widget.defaultTextStyle),
                                decoration: selectedIndex ==
                                        widget.strWeekDays.indexOf(i)
                                    ? widget.selectedBackgroundDecoration
                                    : widget.unselectedBackgroundDecoration,
                              )
                            : new Container()
                      ],
                    ),
                  ));
            }).toList())
      ],
    );
    return new Container(
      padding: new EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
      child: rowWeeks,
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: currentDate,

        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != currentDate)
      setState(() {
          currentDate = picked;
      });
  }
}
