import 'package:flutter/material.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/user_address_bloc.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/ui/user_address.dart';

import 'UnicornOutlineButton.dart';

class UserProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    // TODO: implement build
    return new Scaffold(
      backgroundColor: Colors.grey.withOpacity(0.12),
      body: new Container(
          child: ListView.builder(
        itemCount: 13,
        itemBuilder: (context, index) {
          if (index == 0) {
            return new Card(
              child: Stack(
                children: <Widget>[
                  new Container(
                    decoration: BoxDecoration(
                      gradient: CustomColors.yellowGradientForText,
                    ),
                    height: 90,
                  ),
                  new Container(
                    child: new ClipOval(
                      child: Container(
                        color: Colors.white,
                        width: 94,
                        height: 94,
                      ),
                    ),
                    margin: EdgeInsets.only(top: 45),
                    alignment: Alignment.center,
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 47),
                    child: new ClipOval(
                      child: Image.network(
                        parseJwt()["imageUrl"] == null
                            ? "https://lh3.googleusercontent.com/Z4uHbjIjELCl1WaD_fGK7VnkE9sXzT_b4SLAmmapf7JbmtR9pNF17JW58z3vA7JsRuCGl0BiovQc7zuHiuOp6PZi7Cw=w70-h72-cc"
                            : parseJwt()["imageUrl"] + "?width=200&height=200",
                        width: 90,
                        height: 90,
                        fit: BoxFit.cover,
                      ),
                    ),
                    alignment: Alignment.center,
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 90.0+60),
                    child: new Column(
                      children: <Widget>[
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              "icons/avatar_outline.png",
                              width: 16,
                              height: 16,
                              color: Colors.grey,
                            ),
                            new Container(
                              margin: EdgeInsets.only(left: 8.0),
                            ),
                            new Text(
                              parseJwt()["name"],
                              style: customMediumBoldTextStyle(
                                  fontSize: 16.0, color: Colors.grey),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 4),
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              "icons/email.png",
                              width: 16,
                              height: 16,
                              color: Colors.grey,
                            ),
                            new Container(
                              margin: EdgeInsets.only(left: 8.0),
                            ),
                            new Text(
                              parseJwt()["email"],
                              style: customMediumBoldTextStyle(
                                  fontSize: 16.0, color: Colors.grey),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 4),
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              "icons/phone-call.png",
                              width: 16,
                              height: 16,
                              color: Colors.grey,
                            ),
                            new Container(
                              margin: EdgeInsets.only(left: 8.0),
                            ),
                            new Text(
                              parseJwt()["mobile"],
                              style: customMediumBoldTextStyle(
                                  fontSize: 16.0, color: Colors.grey),
                              textAlign: TextAlign.center,
                            ),

                          ],
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 12),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            );
          }
          if (index == 1) {
            return new Card(
              margin: EdgeInsets.only(top: 22),
              child: new Column(
                children: <Widget>[
                  new InkWell(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (BuildContext context) {
                        return BlocProvider<UserAddressBloc>(
                          bloc: UserAddressBloc(),
                          child: UserAddress(),
                        );
                      }));
                    },
                    child: new Container(
                      child: new Row(
                        children: <Widget>[
                          new Container(
                            child: Image.asset(
                              "icons/address.png",
                              width: 16,
                              height: 16,
                              color: Colors.cyan,
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(
                                    color: Colors.grey.withOpacity(0.4),
                                    offset: new Offset(1.0, 1.0),
                                    blurRadius: 6.0,
                                  )
                                ]),
                            padding: EdgeInsets.all(12.0),
                          ),
                          new Container(
                            margin: EdgeInsets.only(left: 32.0),
                          ),
                          new Text(
                            "My Address",
                            style: customMediumBoldTextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.only(left: 22.0,top: 12.0),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 22.0),
                  ),
                  new Container(
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          child: Image.asset(
                            "icons/wallet.png",
                            width: 16,
                            height: 16,
                            color: Colors.pink,
                          ),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              boxShadow: <BoxShadow>[
                                new BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  offset: new Offset(1.0, 1.0),
                                  blurRadius: 6.0,
                                )
                              ]),
                          padding: EdgeInsets.all(12.0),
                        ),
                        new Container(
                          margin: EdgeInsets.only(left: 32.0),
                        ),
                        new Text(
                          "My Wallet",
                          style: customMediumBoldTextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.only(left: 22.0),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 22.0),
                  ),
                  new Container(
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          child: Image.asset(
                            "icons/question.png",
                            width: 16,
                            height: 16,
                            color: Colors.green,
                          ),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              boxShadow: <BoxShadow>[
                                new BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  offset: new Offset(1.0, 1.0),
                                  blurRadius: 6.0,
                                )
                              ]),
                          padding: EdgeInsets.all(12.0),
                        ),
                        new Container(
                          margin: EdgeInsets.only(left: 32.0),
                        ),
                        new Text(
                          "Help/FAQs",
                          style: customMediumBoldTextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.only(left: 22.0),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 22.0),
                  ),
                  new Container(
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          child: Image.asset(
                            "icons/share.png",
                            width: 16,
                            height: 16,
                            color: Colors.purple,
                          ),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              boxShadow: <BoxShadow>[
                                new BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  offset: new Offset(1.0, 1.0),
                                  blurRadius: 6.0,
                                )
                              ]),
                          padding: EdgeInsets.all(12.0),
                        ),
                        new Container(
                          margin: EdgeInsets.only(left: 32.0),
                        ),
                        new Text(
                          "Share the App",
                          style: customMediumBoldTextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.only(left: 22.0),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 12.0),
                  )

                ],
              ),
            );
          }


          if (index == 2) {
            return new Card(
              margin: EdgeInsets.only(top: 22),
              child: new InkWell(
                onTap: (){},

                child: new Container(
                  child: new Text("Logout",textAlign: TextAlign.center,style: customMediumBoldTextStyle(color: Colors.grey),),
                  padding: EdgeInsets.all(12),
                ),
              )
            );
          }
        },
      )),
    );
  }
}
