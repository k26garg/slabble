import 'dart:io';
import 'package:flutter/material.dart';
import 'package:slabble/blocs/address_bloc.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/model/create_address_model.dart';
import 'package:slabble/model/location_result.dart';

class CreateAddressUI extends StatefulWidget {
  LocationResult locationResult;
  String addressType;

  CreateAddressUI(this.locationResult,this.addressType);

  @override
  _CreateAddressUIState createState() => _CreateAddressUIState();
}

class _CreateAddressUIState extends State<CreateAddressUI> {
  AddressBloc addressBloc;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    addressBloc = BlocProvider.of<AddressBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color(0xFFf6f6f6),
        appBar: new AppBar(
          title: new Text("CREATE ADDRESS",style: customRegularTextStyle(color: Colors.black),),
          backgroundColor: Colors.white,
          leading: new GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: new Icon(Icons.arrow_back,color: Colors.black),
          ),
        ),
        body: new Stack(
          children: <Widget>[
            new ListView.builder(
              itemCount: 1,
              itemBuilder: (context, index) {
                if (index == 0) {
                  return new Card(
                      margin: EdgeInsets.only(
                          left: 8.0, right: 8.0, top: 16.0, bottom: 16.0),
                      elevation: 2.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(0.0))),
                      child: new Container(
                        padding: EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              child: new Text("ADD NEW ADDRESS",
                                  style: customMediumTextStyle(
                                      color: Colors.black, fontSize: 12.0)),
                              margin: EdgeInsets.only(bottom: 8.0),
                            ),
                            new StreamBuilder(
                              stream: addressBloc.getFlatNo,
                              builder: (context, snapshot) {
                                return new Container(
                                  margin:
                                  EdgeInsets.only(bottom: 8.0, top: 8.0),
                                  child: new TextField(
                                    keyboardType: TextInputType.text,
                                    decoration: new InputDecoration(
                                      border: new OutlineInputBorder(
                                        borderRadius:
                                        BorderRadius.circular(0.0),
                                      ),
                                      focusedBorder: new OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(0.0)),
                                      hintText: 'Flat No./Building No.*',
                                      hintStyle: customMediumTextStyle(
                                          fontSize: 12.0,
                                          color:
                                          Colors.black.withOpacity(0.54)),
                                      errorText: snapshot.error,
                                      contentPadding: EdgeInsets.all(14.0),
                                    ),
                                    onChanged: (value) {
                                      addressBloc.changeFlatNo(value);
                                    },
                                  ),
                                );
                              },
                            ),
                            new StreamBuilder(
                              stream: addressBloc.getLandmark,
                              builder: (context, snapshot) {
                                return new Container(
                                  margin:
                                  EdgeInsets.only(bottom: 8.0, top: 8.0),
                                  child: new TextField(
                                    keyboardType: TextInputType.text,
                                    decoration: new InputDecoration(
                                      border: new OutlineInputBorder(
                                        borderRadius:
                                        BorderRadius.circular(0.0),
                                      ),
                                      focusedBorder: new OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(0.0)),
                                      hintText: 'Landmark*',
                                      hintStyle: customMediumTextStyle(
                                          fontSize: 12.0,
                                          color:
                                          Colors.black.withOpacity(0.54)),
                                      errorText: snapshot.error,
                                      contentPadding: EdgeInsets.all(14.0),
                                    ),
                                    onChanged: (value) {
                                      addressBloc.changeLandmark(value);
                                    },
                                  ),
                                );
                              },
                            ),
                            new Container(
                              margin: EdgeInsets.only(bottom: 8.0, top: 8.0),
                              child: new TextField(
                                keyboardType: TextInputType.text,
                                enabled: false,
                                controller: TextEditingController(
                                    text: widget.locationResult.description),
                                maxLines: 3,
                                decoration: new InputDecoration(
                                    border: new OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                    ),
                                    focusedBorder: new OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(0.0)),
                                    hintText: 'Address*',
                                    hintStyle: customMediumTextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black.withOpacity(0.54)),
                                    contentPadding: EdgeInsets.all(14.0)),

                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(bottom: 8.0, top: 8.0),
                              child: new TextField(
                                controller: TextEditingController(
                                    text: widget.locationResult.city),
                                enabled: false,
                                keyboardType: TextInputType.text,
                                decoration: new InputDecoration(
                                    border: new OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                    ),
                                    focusedBorder: new OutlineInputBorder(
                                        borderRadius:
                                        BorderRadius.circular(0.0)),
                                    hintText: 'City*',
                                    hintStyle: customMediumTextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black.withOpacity(0.54)),
                                    contentPadding: EdgeInsets.all(14.0)),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(bottom: 8.0, top: 8.0),
                              child: new TextField(
                                keyboardType: TextInputType.text,
                                enabled: false,
                                controller: TextEditingController(
                                    text: widget.locationResult.state),
                                decoration: new InputDecoration(
                                    border: new OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                    ),
                                    focusedBorder: new OutlineInputBorder(
                                        borderRadius:
                                        BorderRadius.circular(0.0)),
                                    hintText: 'State*',
                                    hintStyle: customMediumTextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black.withOpacity(0.54)),
                                    contentPadding: EdgeInsets.all(14.0)),

                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(bottom: 8.0, top: 8.0),
                              child: new TextField(
                                controller: TextEditingController(
                                    text: widget.locationResult.postalCode),
                                enabled: false,
                                keyboardType: TextInputType.number,
                                maxLength: 6,
                                decoration: new InputDecoration(
                                    border: new OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                    ),
                                    focusedBorder: new OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(0.0)),
                                    hintText: 'Pincode*',
                                    hintStyle: customMediumTextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black.withOpacity(0.54)),
                                    contentPadding: EdgeInsets.all(14.0)),
                              ),
                            ),


                          ],
                        ),
                      ));
                }
              },
            ),
            new StreamBuilder(
              stream: addressBloc.loading,
              builder: (context, snapshot) {
                if (snapshot.data != null && snapshot.data) {
                  return new Container(
                    child: new CircularProgressIndicator(),
                    alignment: Alignment.center,
                    color: CustomColors.lightBlack,
                  );
                } else {
                  return new Container();
                }
              },
            )
          ],
        ),
        bottomNavigationBar: new Container(
          child: new RaisedButton(
            onPressed: () {
              CreateAddressModel model = addressBloc.createAddressModel();
              model.latitude = widget.locationResult.latitude;
              model.longitude = widget.locationResult.longitude;
              model.city = widget.locationResult.city;
              model.state = widget.locationResult.state;
              model.addressText = widget.locationResult.description;
              model.pincode = widget.locationResult.postalCode;
              model.type=widget.addressType;

              String text;

              if (model != null) {
                _onCreateAddress(model).then((value) {
                  text = value["text"];
                }).catchError((onError) {
                  WidgetsBinding.instance.addPostFrameCallback((_) =>
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text(onError),
                          duration: Duration(seconds: 5))));
                }).whenComplete(() {
                  addressBloc.loader.sink.add(false);
                  Navigator.pop(context);
                  Navigator.pop(context,text);
                });
              }
            },
            color: CustomColors.darkYellow,
            child: new Text("Submit",
                style: customRegularTextStyle(color: Colors.black)),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            padding: EdgeInsets.all(14.0),
          ),
          margin: EdgeInsets.only(left: 22.0, right: 22.0, bottom: 22.0),
        ));
  }

  Future<Map<String, dynamic>> _onCreateAddress(
      CreateAddressModel createAddressModel) async {
    return await ApiRequest.allPostApi(
            url: "api/v1/user/address",
            postParams: createAddressModel.toJson(),
            headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .catchError((onError) {
      throw onError;
    });
  }
}
