import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/order_bloc.dart';
import 'package:slabble/common/api_request.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/model/cart.dart';
import 'package:slabble/model/packages.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/request/create_cart_request.dart';
import 'package:slabble/ui/add-address.dart';
import 'calender_widget.dart';

class OrderService extends StatefulWidget {
  Cart cart;

  OrderService(this.cart);

  @override
  _OrderServiceState createState() => _OrderServiceState();
}

class _OrderServiceState extends State<OrderService> {
  OrderBloc orderBloc;

  @override
  void initState() {
    // TODO: implement initState
    orderBloc = BlocProvider.of<OrderBloc>(context);
    orderBloc.cart.sink.add(widget.cart);
    orderBloc.bathRoomCount.sink.add(widget.cart.items[0].no_of_bathroom);
    orderBloc.bedRoomCount.sink.add(widget.cart.items[0].no_of_bedroom);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    DateTime dateCallback;

    return new Scaffold(
      body: new Container(
          child: new Container(
        padding: EdgeInsets.all(8.0),
        child: ListView.builder(
          itemCount: 5,
          itemBuilder: (context, index) {
            if (index == 0) {
              return new GridView.count(
                crossAxisCount: 2,
                childAspectRatio: 1.6,
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                children: <Widget>[
                  new Card(
                    child: new Container(
                        margin: EdgeInsets.only(left: 8.0, right: 8.0),
                        child: new Row(
                          children: <Widget>[
                            new Column(
                              children: <Widget>[
                                new Image.network(
                                  "https://lh3.googleusercontent.com/oiQJuDRTYyQttrjNoql4t-S169xaW2QZiRpGbJTWGIMbP227YLTpeAqFS95FFu5_FGsDlq3PiOSwZEU4V1ZfyskOIg",
                                  width: 50,
                                  height: 50,
                                ),
                                new Text(
                                  "Bathroom",
                                  style: customRegularTextStyle(),
                                )
                              ],
                              crossAxisAlignment:
                              CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                            ),
                            new StreamBuilder(
                              stream: orderBloc.getCart,
                              builder: (context, snapshot) {
                                if(snapshot.connectionState==ConnectionState.active && snapshot.data!=null){
                                  Cart cart = snapshot.data;
                                  return new Expanded(
                                    child: new Column(
                                      children: <Widget>[
                                        new GestureDetector(
                                          child: new Icon(
                                            Icons.add_circle,
                                            color: CustomColors.green,
                                          ),
                                          onTap: () {
                                            updateCart(cart.items[0].product.commodityId, cart.items[0].no_of_bathroom+1, cart.items[0].no_of_bedroom);
                                          },
                                        ),
                                        new Text(cart.items[0].no_of_bathroom.toString()),
                                        new GestureDetector(
                                          child: new Icon(
                                            Icons.remove_circle,
                                            color: CustomColors.green,
                                          ),
                                          onTap: () {
                                            updateCart(cart.items[0].product.commodityId, cart.items[0].no_of_bathroom-1, cart.items[0].no_of_bedroom);
                                          },
                                        )
                                      ],
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                    ),
                                  );
                                }else{
                                  return new Container();
                                }
                              },
                            )
                          ],
                        )),
                  ),
                  new Card(
                      child: new Container(
                        margin: EdgeInsets.only(left: 8.0, right: 8.0),
                        child: new Row(
                          children: <Widget>[
                            new Column(
                              children: <Widget>[
                                new Image.network(
                                  "https://lh3.googleusercontent.com/AuIL0mW6oyjr1nqEzQqLBiozadUoxSIpsmGfjcXSFwMKymiduQssiDtgUw72z45OP_CarK-JI0R5BLiPryewsdjyKQ",
                                  width: 50,
                                  height: 50,
                                ),
                                new Text(
                                  "Bedroom",
                                  style: customRegularTextStyle(),
                                )
                              ],
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                            ),
                            new StreamBuilder(
                              stream: orderBloc.getCart,
                              builder: (context, snapshot) {
                                if(snapshot.connectionState==ConnectionState.active && snapshot.data!=null){
                                  Cart cart = snapshot.data;
                                  return new Expanded(
                                    child: new Column(
                                      children: <Widget>[
                                        new GestureDetector(
                                          child: new Icon(
                                            Icons.add_circle,
                                            color: CustomColors.green,
                                          ),
                                          onTap: () {
                                            updateCart(cart.items[0].product.commodityId, cart.items[0].no_of_bathroom, cart.items[0].no_of_bedroom+1);
                                          },
                                        ),
                                        new Text(cart.items[0].no_of_bedroom.toString()),
                                        new GestureDetector(
                                          child: new Icon(
                                            Icons.remove_circle,
                                            color: CustomColors.green,
                                          ),
                                          onTap: () {
                                            updateCart(cart.items[0].product.commodityId, cart.items[0].no_of_bathroom, cart.items[0].no_of_bedroom-1);
                                          },
                                        )
                                      ],
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                    ),
                                  );
                                }
                                else{
                                  return new Container();
                                }
                              },
                            )
                          ],
                        ),
                      )),
                ],
              );
            }

            if (index == 1) {
              return new Container(
                child: new Text(
                  "Date",
                  style: customMediumTextStyle(fontSize: 14.0),
                ),
                margin: EdgeInsets.only(top: 8.0),
              );
            }

            if (index == 2) {
              return new Card(
                child: G2xSimpleWeekCalendar(
                  new DateTime.now(),
                  dateCallback: (date) => dateCallback,
                  defaultTextStyle: customRegularTextStyle(
                      color: Colors.black, fontSize: 12.0),
                  selectedTextStyle: customMediumTextStyle(
                      color: Colors.black, fontSize: 12.0),
                  selectedBackgroundDecoration: BoxDecoration(
                      color: CustomColors.yellow, shape: BoxShape.circle),
                  unselectedBackgroundDecoration: BoxDecoration(
                      color: CustomColors.lightGrey,
                      shape: BoxShape.circle),
                ),
              );
            }
          },
        )
      )),
      appBar: PreferredSize(
          child: new AppBar(
            flexibleSpace: new Container(
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Text(
                          widget.cart.items[0].product.name,
                          style: customRegularTextStyle(
                              color: Colors.black, fontSize: 26.0),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 12.0),
                        ),
                        new Text(
                          "Basic package given by slabble",
                          style: customRegularTextStyle(
                              color: Colors.black, fontSize: 12.0),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 12.0),
                        ),
                      ],
                    ),
                    flex: 2,
                  ),
                  new Expanded(
                    child: new Image.asset(
                      "icons/forklift.png",
                      width: 64,
                      height: 64,
                    ),
                    flex: 3,
                  )
                ],
              ),
              margin: EdgeInsets.only(left: 16.0),
            ),
            leading: new Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
              size: 16.0,
            ),
            backgroundColor: Colors.white,
          ),
          preferredSize: Size.fromHeight(160)),
      bottomNavigationBar: new StreamBuilder(
        stream: orderBloc.getCart,
        builder: (context,snapshot){
          if(snapshot.connectionState==ConnectionState.active && snapshot.data!=null){
            Cart cart = snapshot.data;
            return new Container(
              child: new RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) {
                        return AddAddress();
                      },
                      fullscreenDialog: true));
                },
                color: CustomColors.darkYellow,
                child: new Text("Confirm Address | " + formatCurrency.format(cart.items[0].totalAmount).toString(),
                    style: customRegularTextStyle(color: Colors.black)),
                shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                padding: EdgeInsets.all(14.0),
              ),
              margin: EdgeInsets.only(left: 22.0, right: 22.0, bottom: 22.0),
            );
          }
          else{
            return new Container();
          }
        },
      ),
    );
  }

  updateCart(int packageId, int minBathRomCount, int minBedRoomCount) async {
    orderBloc.showLoader.sink.add(true);
    CreateCartRequest cartRequest =
        new CreateCartRequest(packageId, minBathRomCount, minBedRoomCount);
    dynamic response = await ApiRequest.allPostApi(
            url: "api/v1/user/update-cart",
            postParams: cartRequest.toJson(),
            headers: {HttpHeaders.contentTypeHeader: 'application/json'})
        .then((value) {
      Cart cart = Cart.fromJson(value);
      orderBloc.showLoader.sink.add(false);
      orderBloc.cart.sink.add(cart);
    });

    return response;
  }
}
