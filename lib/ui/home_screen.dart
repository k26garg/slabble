import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:slabble/blocs/bloc_provider.dart';
import 'package:slabble/blocs/home_bloc.dart';
import 'package:slabble/blocs/search_location_bloc.dart';
import 'package:slabble/blocs/service_bloc.dart';
import 'package:slabble/model/packages.dart';
import 'package:slabble/common/constants.dart';
import 'package:slabble/common/custom_color.dart';
import 'package:slabble/model/services.dart';
import 'package:slabble/ui/search_location.dart';
import 'package:slabble/ui/service_screen.dart';
import 'package:flutter/services.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeBloc homeScreenBloc;
  int selectedIndex = 0;

  String currentAddressText;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    homeScreenBloc = BlocProvider.of<HomeBloc>(context);
    getCurrentAddress();
    super.didChangeDependencies();
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: currentAddressText != null
          ? new AppBar(
              elevation: 1.0,
              title: new InkWell(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Text(
                          "Location",
                          style: customMediumBoldTextStyle(color: Colors.black),
                        ),
                        new Icon(
                          Icons.keyboard_arrow_down,
                          color: Colors.black,
                        )
                      ],
                    ),
                    new Text(
                      currentAddressText,
                      style: customRegularTextStyle(
                          color: Colors.grey, fontSize: 14.0),
                    )
                  ],
                ),
                onTap: () async {
                  var result = await Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return BlocProvider<SearchLocationBloc>(
                      bloc: SearchLocationBloc(),
                      child: SearchLocation("current"),
                    );
                  }));
                  setState(() {
                    if (result != null) {
                      currentAddressText = result;
                    }
                  });
                },
              ))
          : new AppBar(
              elevation: 0.0,
            ),
      backgroundColor: Colors.white,
      body: StreamBuilder(
        stream: homeScreenBloc.homeService,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            List packageList = snapshot.data.A["packages"];
            List serviceList = snapshot.data.B["services"];

            List<Packages> packages =
                packageList.map((m) => new Packages.fromJson(m)).toList();
            List<Services> services =
                serviceList.map((m) => new Services.fromJson(m)).toList();

            return new Container(
                margin: EdgeInsets.all(12.0),
                child: new ListView.builder(
                  itemCount: 6,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return new Text("Packages",style: customMediumBoldTextStyle(color: CustomColors.green),);
                    }
                    if (index == 1) {
                      return new Container(
                        margin: EdgeInsets.only(top: 2.0),
                      );
                    }
                    if (index == 2) {
                      return new Container(
                        height: 150,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: packages.map((m) {
                            return new Container(
                              margin: EdgeInsets.only(right: 8.0),
                              child: new Stack(
                                children: <Widget>[

                                  new InkWell(
                                    child: new ClipRRect(
                                      borderRadius:
                                      new BorderRadius.circular(8.0),
                                      child: new Image.network(
                                        m.imageUrl,
                                        width: 200,
                                        height: 200,
                                      ),
                                    ),
                                    onTap: (){
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) {
                                                return BlocProvider<ServiceBloc>(
                                                  bloc: ServiceBloc(m.packageId),
                                                  child: ServiceScreen(m),
                                                );
                                              }));

                                    },
                                    splashColor: Colors.white,
                                  ),
                                  new Container(
                                    child: new Text(
                                      m.name,
                                      textAlign: TextAlign.center,
                                      style: customMediumBoldTextStyle(
                                          color: Colors.white,fontSize: 20.0),
                                    ),
                                    alignment: Alignment.bottomCenter,
                                    width: 200,
                                    margin: EdgeInsets.only(bottom: 8.0),
                                  ),

                                ],
                              ),
                            );
                          }).toList(),
                        ),
                      );
                    }
                    if (index == 3) {
                      return new Text("Services",style: customMediumBoldTextStyle(color: CustomColors.green),);

                    }

                    if (index == 4) {
                      return new Container(
                        color: Colors.grey.withOpacity(0.08),
                        child: new GridView.count(
                          crossAxisCount: 3,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          childAspectRatio: 1.0,
                          children: services.map((Services service) {
                            return new Card(
                              elevation: 2.0,
                              child: new Container(
                                child: new InkWell(
                                    onTap: () {
                                      /*Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) {
                                      return BlocProvider<ServiceBloc>(
                                        bloc: ServiceBloc(service.),
                                        child: ServiceScreen(p),
                                      );
                                    }));*/
                                    },
                                    splashColor: CustomColors.yellow,
                                    child: new Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Container(
                                          child: new Image.network(service.image,
                                              width: 32, height: 32),
                                          margin: EdgeInsets.all(12.0),
                                        ),
                                        new Expanded(
                                          child: new Container(
                                            child: new Text(
                                              service.name,
                                              maxLines: 2,
                                              style: customRegularTextStyle(
                                                  fontSize: 14.0),
                                            ),
                                            margin: EdgeInsets.only(
                                                left: 12.0,
                                                right: 12.0,
                                                bottom: 12.0),
                                          ),
                                        )
                                      ],
                                    )),
                              ),
                            );
                          }).toList(),
                        ),
                      );
                    }
                    if(index==5){
                      return new Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          border: Border.all(color: Colors.grey)
                        ),
                        margin: EdgeInsets.only(top: 12.0),
                        padding: EdgeInsets.all(12.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text("Refer & Earn",style: customMediumBoldTextStyle(color: Colors.black),),
                                new Text("Win upto 250 coins by \nrefer the code",style: customRegularTextStyle(color: Colors.grey),)
                              ],
                            ),
                            new Expanded(
                              child: new Container(
                                margin: EdgeInsets.only(right: 36.0),
                                child: new Image.asset("icons/dollar.png",width: 50,height: 50,alignment: Alignment.centerRight,),
                              ),
                            )
                          ],
                        ),
                      );
                    }
                  },
                ));
          } else {
            return new Container();
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    homeScreenBloc.dispose();
    super.dispose();
  }

  Future getCurrentAddress() async {
    Location location = new Location();
    String error;
    LocationData myLocation;
    try {
      myLocation = await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'please grant permission';
        print(error);
      }
      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'permission denied- please enable it from app settings';
        print(error);
      }
      myLocation = null;
    }
    final coordinates =
        new Coordinates(myLocation.latitude, myLocation.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    var addressLine = first.addressLine;
    setState(() {
      currentAddressText = addressLine;
    });
  }
}
