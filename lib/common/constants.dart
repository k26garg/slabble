import 'package:flutter/material.dart';
import 'package:slabble/common/preferences.dart';
import 'package:intl/intl.dart';
import 'custom_color.dart';
import 'dart:convert';


final String baseUrl = 'http://52.220.50.106:9100/';

String authToken = Preferences.getData(Preferences.KEY_AUTH_TOKEN);

final formatCurrency = new NumberFormat.currency(
    locale: "en-IN", symbol: "\u20B9", decimalDigits: 2);

var dateFormat = new DateFormat("d MMM yyy");

Map<String, dynamic> parseJwt() {
  final parts = authToken.split('.');
  if (parts.length != 3) {
    throw Exception('invalid token');
  }

  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }
  return payloadMap;
}

String _decodeBase64(String str) {
  String output = str.replaceAll('-', '+').replaceAll('_', '/');

  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw Exception('Illegal base64url string!"');
  }

  return utf8.decode(base64Url.decode(output));
}

buildSlabbleTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
      textTheme: customTextTheme(base.textTheme),
      primaryIconTheme: customIconThemeForAppBar(base.iconTheme),
      primaryTextTheme: customTextThemeForAppBar(base.textTheme),
      accentColor: CustomColors.yellow,
      primaryColor: Colors.white,
      bottomAppBarColor: Colors.white,
      cursorColor: Color(0xFF979797),
      primaryColorDark: Color(0xFF979797),
      splashColor: CustomColors.yellow,);
}

customTextTheme(TextTheme base) {
  return base.copyWith(
    title: customMediumBoldTextStyle(color: Colors.black),
    body1: customMediumTextStyle(color: Colors.black),
    button: customMediumTextStyle(),
  );
}

customTextThemeForAppBar(TextTheme base) {
  return base.copyWith(
    title:
        customRegularTextStyle(color: Colors.white, fontSize: 18.0),
  );
}

customIconThemeForAppBar(IconThemeData base) {
  return base.copyWith(
    color: Colors.white,
  );
}

customRegularTextStyle({Color color, double lineHeight, double fontSize}) {
  return TextStyle(
      fontSize: fontSize == null ? 16.0 : fontSize,
      fontFamily: "Roboto",
      height: lineHeight == 0.0 ? 0.0 : lineHeight,
      fontWeight: FontWeight.w400,
      color: color != null ? color : Colors.black);
}

customMediumTextStyle({Color color, double lineHeight, double fontSize}) {
  return TextStyle(
      fontSize: fontSize == null ? 16.0 : fontSize,
      fontFamily: "Roboto",
      height: lineHeight == 0.0 ? 0.0 : lineHeight,
      fontWeight: FontWeight.w500,
      color: color != null ? color : Colors.black);
}

customMediumBoldTextStyle({Color color, double fontSize, double lineHeight}) {
  return TextStyle(
      fontSize: fontSize == null ? 16.0 : fontSize,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w700,
      color: color != null ? color : Colors.black,
      height: lineHeight == 0.0 ? 0.0 : lineHeight);


}

customBoldTextStyle({Color color, double fontSize, double lineHeight}) {
  return TextStyle(
      fontSize: fontSize == null ? 16.0 : fontSize,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w900,
      color: color != null ? color : Colors.black,
      height: lineHeight == 0.0 ? 0.0 : lineHeight);


}