import 'package:slabble/common/constants.dart';
import 'package:slabble/network/http_param_object.dart';
import 'package:slabble/network/http_service.dart';

class ApiRequest {
  static Future<dynamic> allPostApi({String url, Map<String, String> headers,
    String method,  Map<String, dynamic>  postParams}) {
    HttpParamObject httpParamObject = new HttpParamObject();
    httpParamObject.url = baseUrl + url;
    httpParamObject.headers = headers;
    httpParamObject.method = 'post';
    httpParamObject.postParam = postParams;

    return HttpService.getData(httpParamObject);
  }

  static Future<dynamic> allPutApi({String url, Map<String, String> headers,
    String method,  Map<String, dynamic>  postParams}) {
    HttpParamObject httpParamObject = new HttpParamObject();
    httpParamObject.url = baseUrl + url;
    httpParamObject.headers = headers;
    httpParamObject.method = 'put';
    httpParamObject.postParam = postParams;

    return HttpService.getData(httpParamObject);
  }
  static Future<dynamic> allGetApi({String url, Map<String, String> headers,
    String method}){

    HttpParamObject httpParamObject = new HttpParamObject();
    httpParamObject.url = baseUrl  + url;
    httpParamObject.headers = headers;
    httpParamObject.method = 'get';


    return HttpService.getData(httpParamObject);

  }

  static Future<dynamic> getLocationFromSearch({String url, Map<String, String> headers,
    String method}){

    HttpParamObject httpParamObject = new HttpParamObject();
    httpParamObject.url = url;
    httpParamObject.headers = headers;
    httpParamObject.method = 'get';


    return HttpService.getData(httpParamObject);

  }

}
