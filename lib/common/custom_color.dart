import 'package:flutter/material.dart';

class CustomColors {
  static Color primaryAppColor = Color(0xFF293875);
  static Color cardBackground = Color(0x2938751a);
  static Color darkGrey = Color(0xFF979797);
  static Color mediumDarkGrey = Color(0xFF979797).withOpacity(0.5);
  static Color lightBlack = Colors.black.withOpacity(0.2);
  static Color rippleColor = Color(0xFFD3D3D3);
  static Color indicatorColor = Colors.white.withOpacity(0.3);
  static Color selectedIndicatorColor = Colors.white;
  static Color yellow = Color(0xFFFCD55C);
  static Color darkYellow =Color(0xFFFFC740);
  static Color green = Color(0xFF4ec994);
  static Color lightGrey = Color(0xFFf6f6f6);
  static Color stripGrey = Color(0xFFf2f2f2);
  static Color red = Color(0xFFd44444);
  static Color purple = Color(0xFF0336FF);

  static Color logoTextColor = Color(0xFF5B5B5E);

  static Color yellow1 = Color(0xFFFFE345);
  static Color yellow2 = Color(0xFFFFD233);
  static Color yellow3 = Color(0xFFFFC221);

  static const yellowGradientForText = const LinearGradient(
      colors: [Color(0xFFFFE345), Color(0xFFFFD233),Color(0xFFFFC221)],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter
  );
}
