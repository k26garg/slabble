import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static final String KEY_AUTH_TOKEN = "Authorization";
  static final String USER_LOGIN_TYPE = "LoginType";
  static final String CURRENT_LOCATION = "CurrentLocation";
  static SharedPreferences preferences;

  static initPreference() async {
    preferences =  await SharedPreferences.getInstance();
  }

  static saveData(String key, String value) {
    preferences.setString(key, value);
  }

  static saveBoolData(String key, bool value) {
    preferences.setBool(key, value);
  }

  static getData(String key) {
    return preferences.get(key);
  }

  static removeData(String key) {
    return preferences.remove(key);
  }

}
